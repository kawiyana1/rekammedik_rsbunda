﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class RekamMedikODCViewModel
    {
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string Kode_Supplier { get; set; }
        public string Nama_Supplier { get; set; }
        public string Perusahaan { get; set; }
        public string JenisKelamin { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string ALamat { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglKeluar { get; set; }
        public string TglKeluar_View { get; set; }
        public bool SudahCoding { get; set; }
    }
}