﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class KelengkapanDokumenViewModel
    {
        public string Kode_dokumen { get; set; }
        public string Nama_Dokumen { get; set; }
        public string Keterangan { get; set; }
        public bool Active { get; set; }
        public bool Dok_RI { get; set; }
        public bool Dok_RJ { get; set; }
    }
}