﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class DetailRegistrasiViewModel
    {
        public string NoReg { get; set; }
        public string Tanggal { get; set; }
        public string Jam { get; set; }
    }
}