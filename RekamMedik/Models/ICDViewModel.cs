﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class ICDViewModel
    {
        public string KodeICD { get; set; }
        public string Descriptions { get; set; }
    }
}