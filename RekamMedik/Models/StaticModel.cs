﻿using RekamMedik.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RekamMedik.Models
{
    public class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        public static List<SelectListItem> SelectListFormatReport
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "PDF", Value = "PDF"},
                    new SelectListItem() { Text = "Excel", Value = "Excel" },
                    new SelectListItem() { Text = "Word", Value = "Word" }
                };
            }
        }

    }
}