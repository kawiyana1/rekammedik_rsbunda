﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class DokumenViewModel
    {
        public string No_Registrasi { get; set; }
        public string Kode_Dokumen { get; set; }
        public string Nama_Dokument { get; set; }
        public bool Avilable { get; set; }
    }
}