﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class TindakanViewModel
    {
        public string KDTdk { get; set; }
        public string Tindakan { get; set; }
    }
}