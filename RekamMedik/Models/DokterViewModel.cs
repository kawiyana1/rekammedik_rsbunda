﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class DokterViewModel
    {
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
    }
}