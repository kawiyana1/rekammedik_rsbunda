﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class RekamMedikRIInsertViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string Agama { get; set; }
        public string Pekerjaan { get; set; }
        public string Pendidikan { get; set; }
        public string JenisKelamin { get; set; }
        public string TglLahir { get; set; }
        public string Umur_View { get; set; }
        public Nullable<int> Umur { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglReg { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglKeluar { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan JamReg { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan JamKeluar { get; set; }
        public string SectionName { get; set; }
        public string NamaKelas { get; set; }
        public Nullable<int> LamaRawat { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan MeninggalJam { get; set; }
        public bool KasusPertama { get; set; }
        public bool KasusBerulang { get; set; }
        public string DiagnosaMasuk { get; set; }
        public string DiagnosaMasukNama { get; set; }
        public bool CaraMasuk_IGD { get; set; }
        public bool CaraMasuk_RJ { get; set; }
        public bool CaraMasuk_RI { get; set; }
        public bool CaraMasuk_OK { get; set; }
        public bool CaraMasuk_VK { get; set; }
        public string DiagnosaUtama { get; set; }
        public string DiagnosaUtamaNama { get; set; }
        public string DiagnosaSekunder { get; set; }
        public string DiagnosaSekunderNama { get; set; }
        public string DiagnosaMorfologi { get; set; }
        public string DiagnosaMorfologiNama { get; set; }
        public string DiagnosaTindakan { get; set; }
        public string DiagnosaTindakanNama { get; set; }
        public string DiagnosaSebabPenyakit { get; set; }
        public string SebabLuarPenyakitNama { get; set; }
        public string PenyebabKematian { get; set; }
        public string Alergi { get; set; }
        public bool Imunisasi_Hepatitis { get; set; }
        public bool Imunisasi_Polio { get; set; }
        public bool Imunisasi_Hep { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public bool KeadaanKeluar_Sembuh { get; set; }
        public bool KeadaanKeluar_BelumSembut { get; set; }
        public bool KeadaanKeluar_Membaik { get; set; }
        public bool KeadaanKeluar_Meninggal48 { get; set; }
        public bool KeadaanKeluar_Meinggal49 { get; set; }
        public bool StatusKeluar_Diijinkan { get; set; }
        public bool StatusKeluar_PulangPaksa { get; set; }
        public bool StatusKeluar_PindahRS { get; set; }
        public bool StatusKeluar_DirujukAtas { get; set; }
        public bool StatusKeluar_DirujukKebawah { get; set; }
        public string UserID { get; set; }
        public string StatusKeluar_PindahRS_Kode { get; set; }
        public string StatusKeluar_PindahRS_KodeNama { get; set; }
        public bool KunjunganBaru { get; set; }
        public bool KunjunganLama { get; set; }

        public ListDetail<ICDViewModel> ICD_Detail_List { get; set; }
        public ListDetail<TindakanViewModel> Tindakan_Detail_List { get; set; }
        public ListDetail<ICDViewModel> PenyakitLuar_Detail_List { get; set; }
        public ListDetail<DokumenViewModel> Dokumen_Detail_List { get; set; }
    }
}