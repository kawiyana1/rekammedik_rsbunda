﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RekamMedik.Models
{
    public class RekamMedikODCInsertViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string Agama { get; set; }
        public string Pekerjaan { get; set; }
        public string Pendidikan { get; set; }
        public string JenisKelamin { get; set; }
        public string TglLahir { get; set; }
        public string Umur_View { get; set; }
        public Nullable<int> Umur { get; set; }
        public Nullable<int> KunjunganKe { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglReg { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglKeluar { get; set; }
        public bool KasusPertama { get; set; }
        public bool KasusBerulang { get; set; }
        public string ICD { get; set; }
        public string ICDName { get; set; }
        public string Morfologi { get; set; }
        public string MorfologiName { get; set; }
        public string Tindakan { get; set; }
        public string TindakanName { get; set; }
        public string Keterangan { get; set; }
        public string IDDokter { get; set; }
        public string NamaDOkter { get; set; }
        public string SebabLuarPenyakit { get; set; }
        public string SebabLuarPenyakitName { get; set; }
        public string PenyebabKematian { get; set; }
        public bool KeadaanKeluar_Sembuh { get; set; }
        public bool KeadaanKeluar_BelumSembut { get; set; }
        public bool KeadaanKeluar_Membaik { get; set; }
        public bool KeadaanKeluar_Meninggal_Bwh48 { get; set; }
        public bool KeadaanKeluar_Meninggal_Ats48 { get; set; }
        public bool StatusKeluar_Diijinkan { get; set; }
        public bool StatusKeluar_PulangPaksa { get; set; }
        public bool StatusKeluar_PindahRS { get; set; }
        public bool StatusKeluar_DirujukAtas { get; set; }
        public bool StatusKeluar_DirujukKebawah { get; set; }
        public string UserID { get; set; }
        public string StatusKeluar_PindahRS_Kode { get; set; }
        public string StatusKeluar_PindahRS_KodeNama { get; set; }
        public bool KunjunganBaru { get; set; }
        public bool KunjunganLama { get; set; }

        public ListDetail<ICDViewModel> ICD_Detail_List { get; set; }
        public ListDetail<TindakanViewModel> Tindakan_Detail_List { get; set; }
        public ListDetail<ICDViewModel> PenyakitLuar_Detail_List { get; set; }
    }
}