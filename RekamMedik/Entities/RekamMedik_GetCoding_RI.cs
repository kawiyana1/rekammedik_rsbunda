//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RekamMedik.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RekamMedik_GetCoding_RI
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public string JenisKelamin { get; set; }
        public string NamaKelas { get; set; }
        public Nullable<int> Umur { get; set; }
        public string Alamat { get; set; }
        public string Agama { get; set; }
        public Nullable<System.DateTime> TglReg { get; set; }
        public Nullable<System.DateTime> JamReg { get; set; }
        public Nullable<System.DateTime> TglKeluar { get; set; }
        public Nullable<System.DateTime> JamKeluar { get; set; }
        public Nullable<int> LamaRawat { get; set; }
        public bool KasusPertama { get; set; }
        public bool KasusBerulang { get; set; }
        public string DiagnosaMasuk { get; set; }
        public string DiagnosaMasukNama { get; set; }
        public bool CaraMasuk_IGD { get; set; }
        public bool CaraMasuk_RJ { get; set; }
        public bool CaraMasuk_RI { get; set; }
        public bool CaraMasuk_OK { get; set; }
        public bool CaraMasuk_VK { get; set; }
        public string DiagnosaUtama { get; set; }
        public string DiagnosaUtamaNama { get; set; }
        public string DiagnosaSekunder { get; set; }
        public string DiagnosaSekunderNama { get; set; }
        public string DiagnosaMorfologi { get; set; }
        public string DiagnosaMorfologiNama { get; set; }
        public string DiagnosaTindakan { get; set; }
        public string DiagnosaTindakanNama { get; set; }
        public string DiagnosaSebabPenyakit { get; set; }
        public string SebabLuarPenyakitNama { get; set; }
        public string PenyebabKematian { get; set; }
        public string Alergi { get; set; }
        public bool Imunisasi_Hepatitis { get; set; }
        public bool Imunisasi_Polio { get; set; }
        public bool Imunisasi_Hep { get; set; }
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public bool KeadaanKeluar_Sembuh { get; set; }
        public bool KeadaanKeluar_BelumSembut { get; set; }
        public bool KeadaanKeluar_Membaik { get; set; }
        public bool KeadaanKeluar_Meninggal_Bwh48 { get; set; }
        public bool KeadaanKeluar_Meninggal_Ats48 { get; set; }
        public Nullable<System.TimeSpan> KeadaanKeluar_Meninggal_Jam { get; set; }
        public bool StatusKeluar_Diijinkan { get; set; }
        public bool StatusKeluar_PulangPaksa { get; set; }
        public bool StatusKeluar_PindahRS { get; set; }
        public bool StatusKeluar_DirujukAtas { get; set; }
        public bool StatusKeluar_DirujukKebawah { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public string StatusKeluar_PindahRS_Kode { get; set; }
    }
}
