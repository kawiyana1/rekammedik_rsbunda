﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RekamMedik.Startup))]
namespace RekamMedik
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
