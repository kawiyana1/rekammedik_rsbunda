﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RekamMedik.Entities;
using RekamMedik.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace RekamMedik.Controllers
{
    [Authorize(Roles = "RekamMedik")]
    public class CodingRJController : Controller
    {
        #region ===== I N D E X
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<RekamMedik_GetListCoding_RJ_Result> proses = s.RekamMedik_GetListCoding_RJ();
                    if (filter[13] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[11]))
                        {
                            proses = proses.Where("TglKeluar >= @0", DateTime.Parse(filter[11]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("TglKeluar <= @0", DateTime.Parse(filter[12]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.NoReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.ALamat)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.SectionName)}.Contains(@0)", filter[7]);
                    if (!string.IsNullOrEmpty(filter[9])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.Nama_Supplier)}.Contains(@0)", filter[9]);
                    if (!string.IsNullOrEmpty(filter[10])) proses = proses.Where($"{nameof(RekamMedik_GetListCoding_RJ_Result.Perusahaan)}.Contains(@0)", filter[10]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RekamMedikRJViewModel>(x));
                    foreach (var x in m)
                    {
                        var rm = s.SIMtrRM_RJ.Where(z => z.NoReg == x.NoReg && z.SectionID == x.SectionID);
                        if (rm.Count() > 0)
                        {
                            x.SudahCoding = true;
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.TglKeluar_View = x.TglKeluar.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string sectionid)
        {
            RekamMedikRJInsertViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.RekamMedik_GetCoding_RJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (m == null)
                    {
                        var m1 = s.RekamMedik_Coding_RJ(noreg, sectionid).FirstOrDefault();
                        if (m1 == null) return HttpNotFound();
                        item = IConverter.Cast<RekamMedikRJInsertViewModel>(m1);
                        item.IDDokter = m1.DokterID;
                        item.NamaDOkter = m1.NamaDokter;
                        item.KunjunganKe = m1.Kunjungan;

                        var dtreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                        if (dtreg != null)
                        {
                            item.Umur_View = dtreg.UmurThn.ToString() + " tahun " + dtreg.UmurBln.ToString() + " bulan " + dtreg.UmurHr + " hari";
                        }
                        var dtpasien = s.mPasien.FirstOrDefault(x => x.NRM == dtreg.NRM);
                        if (dtpasien != null)
                        {
                            item.TglLahir = dtpasien.TglLahir.Value.ToString("dd-MM-yyyy");
                        }
                    }
                    else
                    {
                        item = IConverter.Cast<RekamMedikRJInsertViewModel>(m);
                        item.JenisKelamin = m.JenisKelamin == "F" ? "Perempuan" : m.JenisKelamin == "M" ? "Laki-Laki" : "Other";
                        item.TglLahir = m.TglLahir.Value.ToString("dd-MM-yyyy");
                        item.Umur_View = m.UmurThn.ToString() + " tahun " + m.UmurBln.ToString() + " bulan " + m.UmurHr + " hari";
                        var icd = s.SIMtrRMDiagnosa_Utama.Where(x => x.NoReg == noreg).ToList();
                        var tindakan = s.SIMtrRMDiagnosa_Tindakan.Where(x => x.NoReg == noreg).ToList();
                        var penyakitluar = s.SIMtrDiagnosa_PenyakitLuar.Where(x => x.NoReg == noreg).ToList();
                        item.ICD_Detail_List = new ListDetail<ICDViewModel>();
                        item.Tindakan_Detail_List = new ListDetail<TindakanViewModel>();
                        item.PenyakitLuar_Detail_List = new ListDetail<ICDViewModel>();

                        var dokumen = s.SIMtrRM_Dokumen.Where(x => x.No_Registrasi == noreg).ToList();
                        item.Dokumen_Detail_List = new ListDetail<DokumenViewModel>();

                        var rs = s.mSupplier.FirstOrDefault(x => x.Kode_Supplier == m.StatusKeluar_PindahRS_Kode);
                        if (rs != null)
                        {
                            item.StatusKeluar_PindahRS_Kode = rs.Kode_Supplier;
                            item.StatusKeluar_PindahRS_KodeNama = rs.Nama_Supplier;
                        }

                        foreach (var x in icd)
                        {
                            var diagnosa = s.mICD.FirstOrDefault(z => z.KodeICD == x.Diagnosa);
                            var y = new ICDViewModel()
                            {
                                KodeICD = x.Diagnosa,
                                Descriptions = diagnosa.Descriptions

                            };
                            item.ICD_Detail_List.Add(false, y);
                        }
                        foreach (var x in tindakan)
                        {
                            var diagnosa = s.mICDTindakan.FirstOrDefault(z => z.KDTdk == x.Diagnosa);
                            var y = new TindakanViewModel()
                            {
                                KDTdk = x.Diagnosa,
                                Tindakan = diagnosa.Tindakan

                            };
                            item.Tindakan_Detail_List.Add(false, y);
                        }
                        foreach (var x in penyakitluar)
                        {
                            var diagnosa = s.mICD.FirstOrDefault(z => z.KodeICD == x.Diagnosa);
                            var y = new ICDViewModel()
                            {
                                KodeICD = x.Diagnosa,
                                Descriptions = diagnosa.Descriptions

                            };
                            item.PenyakitLuar_Detail_List.Add(false, y);
                        }
                        foreach (var x in dokumen)
                        {
                            var y = new DokumenViewModel()
                            {
                                Kode_Dokumen = x.Kode_Dokumen,
                                Nama_Dokument = x.Nama_Dokument,
                                Avilable = x.Avilable
                            };
                            item.Dokumen_Detail_List.Add(false, y);
                        }

                        var rekammedis = s.SIMtrRM_RJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (rekammedis != null)
                        {
                            item.KunjunganBaru = (bool)rekammedis.KunjunganBaru;
                            item.KunjunganLama = (bool)rekammedis.KunjunganLama;
                        }
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new RekamMedikRJInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region ==== Validation
                                if (item.ICD_Detail_List == null)
                                    item.ICD_Detail_List = new ListDetail<ICDViewModel>();
                                item.ICD_Detail_List.RemoveAll(x => x.Remove);

                                if (item.Tindakan_Detail_List == null)
                                    item.Tindakan_Detail_List = new ListDetail<TindakanViewModel>();
                                item.Tindakan_Detail_List.RemoveAll(x => x.Remove);

                                if (item.PenyakitLuar_Detail_List == null)
                                    item.PenyakitLuar_Detail_List = new ListDetail<ICDViewModel>();
                                item.PenyakitLuar_Detail_List.RemoveAll(x => x.Remove);
                                #endregion

                                #region ==== Header
                                var data = s.SIMtrRM_RJ.FirstOrDefault(x => x.NoReg == item.NoReg && x.SectionID == item.SectionID);
                                if (data == null)
                                {
                                    var m = IConverter.Cast<SIMtrRM_RJ>(item);
                                    m.UserID = User.Identity.GetUserId();
                                    s.SIMtrRM_RJ.Add(m);
                                }
                                else
                                {
                                    var m = IConverter.Cast<SIMtrRM_RJ>(item);
                                    m.UserID = User.Identity.GetUserId();
                                    s.SIMtrRM_RJ.AddOrUpdate(m);
                                }
                                #endregion

                                #region ==== Detail

                                #region ==== ICD
                                var new_list_icd = item.ICD_Detail_List;
                                var real_list_icd = s.SIMtrRMDiagnosa_Utama.Where(x => x.NoReg == item.NoReg).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list_icd)
                                {
                                    var m = new_list_icd.FirstOrDefault(y => y.Model.KodeICD == x.Diagnosa);
                                    if (m == null) s.SIMtrRMDiagnosa_Utama.Remove(x);
                                }

                                foreach (var x in new_list_icd)
                                {
                                    var _m = real_list_icd.FirstOrDefault(y => y.Diagnosa == x.Model.KodeICD);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.SIMtrRMDiagnosa_Utama.Add(new SIMtrRMDiagnosa_Utama()
                                        {
                                            NoReg = item.NoReg,
                                            Diagnosa = x.Model.KodeICD
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Diagnosa = x.Model.KodeICD;
                                    }
                                }
                                #endregion

                                #region ==== TINDAKAN
                                var new_list_tindakan = item.Tindakan_Detail_List;
                                var real_list_tindakan = s.SIMtrRMDiagnosa_Tindakan.Where(x => x.NoReg == item.NoReg).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list_tindakan)
                                {
                                    var m = new_list_tindakan.FirstOrDefault(y => y.Model.KDTdk == x.Diagnosa);
                                    if (m == null) s.SIMtrRMDiagnosa_Tindakan.Remove(x);
                                }

                                foreach (var x in new_list_tindakan)
                                {
                                    var _m = real_list_tindakan.FirstOrDefault(y => y.Diagnosa == x.Model.KDTdk);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.SIMtrRMDiagnosa_Tindakan.Add(new SIMtrRMDiagnosa_Tindakan()
                                        {
                                            NoReg = item.NoReg,
                                            Diagnosa = x.Model.KDTdk
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Diagnosa = x.Model.KDTdk;
                                    }
                                }
                                #endregion

                                #region ==== PENYAKIT LUAR
                                var new_list_penyakitluar = item.PenyakitLuar_Detail_List;
                                var real_list_penyakitluar = s.SIMtrDiagnosa_PenyakitLuar.Where(x => x.NoReg == item.NoReg).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list_penyakitluar)
                                {
                                    var m = new_list_penyakitluar.FirstOrDefault(y => y.Model.KodeICD == x.Diagnosa);
                                    if (m == null) s.SIMtrDiagnosa_PenyakitLuar.Remove(x);
                                }

                                foreach (var x in new_list_penyakitluar)
                                {
                                    var _m = real_list_penyakitluar.FirstOrDefault(y => y.Diagnosa == x.Model.KodeICD);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.SIMtrDiagnosa_PenyakitLuar.Add(new SIMtrDiagnosa_PenyakitLuar()
                                        {
                                            NoReg = item.NoReg,
                                            Diagnosa = x.Model.KodeICD
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Diagnosa = x.Model.KodeICD;
                                    }
                                }
                                #endregion

                                #region ==== DOKUMEN
                                var new_list_dokumen = item.Dokumen_Detail_List;
                                var real_list_dokumen = s.SIMtrRM_Dokumen.Where(x => x.No_Registrasi == item.NoReg);
                                
                                s.SIMtrRM_Dokumen.RemoveRange(real_list_dokumen);

                                if (new_list_dokumen != null)
                                {
                                    foreach (var x in new_list_dokumen)
                                    {
                                        if (x.Model != null)
                                        {
                                            s.SIMtrRM_Dokumen.Add(new SIMtrRM_Dokumen()
                                            {
                                                No_Registrasi = item.NoReg,
                                                Kode_Dokumen = x.Model.Kode_Dokumen,
                                                Nama_Dokument = x.Model.Nama_Dokument,
                                                Avilable = x.Model.Avilable
                                            });
                                        }
                                    }
                                }
                                #endregion
                                #endregion

                                result = new ResultSS(s.SaveChanges());

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrRM_RJ Update {item.NoReg}{item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

    }
}