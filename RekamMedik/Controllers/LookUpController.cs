﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RekamMedik.Entities;
using RekamMedik.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace RekamMedik.Controllers
{
    [Authorize(Roles = "RekamMedik")]
    public class LookUpController : Controller
    {
        #region ==== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ==== S E L E C T  I C D
        [HttpPost]
        public string SelectListICD(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mICD> proses = s.mICD;
                    var param = filter[0];
                    //if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mICD.Descriptions)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.KodeICD.Contains(param) || x.Descriptions.Contains(param));
                    //if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.KodeICD == param || x.Descriptions == param);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ICDViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== I C D
        [HttpPost]
        public string ListICD(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mICD> proses = s.mICD;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mICD.KodeICD)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mICD.Descriptions)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ICDViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  T I N D A K A N
        [HttpPost]
        public string SelectListTindakan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mICDTindakan> proses = s.mICDTindakan;
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x=> x.KDTdk == param || x.Tindakan == param);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<TindakanViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== T I N D A K A N
        [HttpPost]
        public string ListTindakan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mICDTindakan> proses = s.mICDTindakan;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mICDTindakan.KDTdk)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mICDTindakan.Tindakan)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<TindakanViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  D O K T E R
        [HttpPost]
        public string SelectListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x=> x.Active != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.DokterID.Contains(param) || x.NamaDOkter.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.DokterID,
                        Nama = x.NamaDOkter
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== D O K T E R
        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new DokterViewModel()
                    {
                        DokterID = x.DokterID,
                        NamaDokter = x.NamaDOkter
                    });
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== D O K U M E N
        [HttpPost]
        public string ListDokumen(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmDokumen> proses = s.SIMmDokumen.Where(x=> x.Active == true);
                    if (filter[10] == "RJ")
                    {
                        proses = proses.Where(x => x.Dok_RJ == true);
                    }
                    else if (filter[10] == "RI")
                    {
                        proses = proses.Where(x => x.Dok_RI == true);
                    }
                    else
                    {
                        proses = proses.Where(x => x.Dok_RJ == false && x.Dok_RI == false);
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(SIMmDokumen.Kode_dokumen)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(SIMmDokumen.Nama_Dokumen)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new DokumenViewModel()
                    {
                        Kode_Dokumen = x.Kode_dokumen,
                        Nama_Dokument = x.Nama_Dokumen
                    });
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  S E C T I O N
        [HttpPost]
        public string SelectListSection(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.StatusAktif != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.SectionID.Contains(param) || x.SectionName.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.SectionID,
                        Nama = x.SectionName
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        [HttpPost]
        public string SelectListSectionRI(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.StatusAktif != false && x.TipePelayanan == "RI");
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.SectionID.Contains(param) || x.SectionName.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.SectionID,
                        Nama = x.SectionName
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  K E L A S
        [HttpPost]
        public string SelectListKelas(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmKelas> proses = s.SIMmKelas.Where(x => x.Active != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.NamaKelas.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.NamaKelas,
                        Nama = x.NamaKelas
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  K E L A S
        [HttpPost]
        public string SelectListRS(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier.Where(x => x.KodeKategoriVendor == "V-001");
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.Nama_Supplier.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.Kode_Supplier,
                        Nama = x.Nama_Supplier
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}