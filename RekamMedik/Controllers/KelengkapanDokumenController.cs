﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RekamMedik.Entities;
using RekamMedik.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace RekamMedik.Controllers
{
    [Authorize(Roles = "RekamMedik")]
    public class KelengkapanDokumenController : Controller
    {
        #region ===== I N D E X
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var model = new KelengkapanDokumenViewModel();
            model.Active = true;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new KelengkapanDokumenViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var has = s.SIMmDokumen.Where(x => x.Nama_Dokumen == item.Nama_Dokumen).Count() > 0;
                        if (has) throw new Exception("Nama sudah digunakan");

                        var m = IConverter.Cast<SIMmDokumen>(item);
                        s.SIMmDokumen.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMmDokumen Create {m.Kode_dokumen}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            KelengkapanDokumenViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmDokumen.FirstOrDefault(x => x.Kode_dokumen == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<KelengkapanDokumenViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new KelengkapanDokumenViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var model = s.SIMmDokumen.FirstOrDefault(x => x.Kode_dokumen == item.Kode_dokumen);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.Nama_Dokumen.ToUpper() != item.Nama_Dokumen.ToUpper())
                        {
                            var has = s.SIMmDokumen.Where(x => x.Nama_Dokumen == item.Nama_Dokumen).Count() > 0;
                            if (has) throw new Exception("Nama sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMmDokumen Edit {model.Kode_dokumen}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmDokumen> proses = s.SIMmDokumen;
                    if (filter[0] != "") proses = proses.Where($"{nameof(SIMmDokumen.Kode_dokumen)}.Contains(@0)", filter[0]);
                    if (filter[1] != "") proses = proses.Where($"{nameof(SIMmDokumen.Nama_Dokumen)}.Contains(@0)", filter[1]);
                    if (filter[2] != "") proses = proses.Where($"{nameof(SIMmDokumen.Keterangan)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<KelengkapanDokumenViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}