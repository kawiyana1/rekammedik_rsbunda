﻿using Newtonsoft.Json;
using OfficeOpenXml;
using RekamMedik.Entities;
using RekamMedik.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RekamMedik.Controllers
{
    public class ReportExcelController : Controller
    {
        // GET: ReportExcel
        public ActionResult Index()
        {
            var m = new ReportRLHelperViewModel();
            m.Name = "Dartar Report RL";
            m.RL_List = new List<ExcelInfoViewModel>() {
                new ExcelInfoViewModel()
                {
                    param = "RL 5.1_Pengunjung SIRS",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.2_Kunjungan Rawat Jalan",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.3 10_Besar Penyakit Rawat Inap",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.4 10_Besar Penyakit Rawat Jalan",
                }
            };
            return View(m);
        }

        public ActionResult DownloadReport(string name_xls, DateTime fromdate, DateTime todate)
        {
            //var s_formdate = DateTime.Parse("2022-01-01");
            //var s_todate = DateTime.Parse("2022-06-30");
            //var s_name_xls = "RL 5.1_Pengunjung SIRS";

            var exlRpt = new Configurator(name_xls, fromdate, todate);
            var declareReport = exlRpt.initiator();

            var templateFileInfo = new FileInfo(Path.Combine(Server.MapPath($"~/CrystalReports/ExcelTemp/") + declareReport.filename));
            Stream stream = new MemoryStream();
            if (templateFileInfo.Exists)
            {
                using (ExcelPackage p = new ExcelPackage(templateFileInfo))
                {
                    ExcelWorksheet wsEstimate = p.Workbook.Worksheets[declareReport.worksheet];

                    exlRpt.ExcelReport(wsEstimate);

                    p.SaveAs(stream);
                    stream.Position = 0;
                }
            }
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", declareReport.title + ".xlsx");
        }

    }

    public class Configurator{

        private string _config { get; set; }
        private DateTime _fromdate { get; set; }
        private DateTime _todate { get; set; }

        public Configurator(string config, DateTime fromdate, DateTime todate)
        {
            _config = config;
            _fromdate = fromdate;
            _todate = todate;
        }

        public ExcelInfoViewModel initiator()
        {
            var rpt = declareReport();
            var m_rpt = rpt.FirstOrDefault(x => x.param == _config);
            return m_rpt;
        }

        public ExcelWorksheet ExcelReport(ExcelWorksheet exl)
        {
            if (_config == "RL 5.1_Pengunjung SIRS")
            {
                var no = 1;
                var row = 2;
                using (var s = new SIMEntities())
                {
                    var dt = s.RL_5_1(_fromdate,_todate).ToList();
                    foreach (var x in dt)
                    {
                        //Kode_RS Nama_RS Bulan Tahun   Nama_KabKota Kode_Prov   Nomor JenisKegiatan   Jumlah
                        exl.Cells[$"A{row}"].Value = x.Kode_RS;
                        exl.Cells[$"B{row}"].Value = x.Nama_RS;
                        exl.Cells[$"C{row}"].Value = x.Bulan;
                        exl.Cells[$"D{row}"].Value = x.Tahun;
                        exl.Cells[$"E{row}"].Value = x.Nama_KabKota;
                        exl.Cells[$"F{row}"].Value = x.Kode_Prov;
                        exl.Cells[$"G{row}"].Value = x.Nomor;
                        exl.Cells[$"H{row}"].Value = x.JenisKegiatan;
                        exl.Cells[$"I{row}"].Value = x.Jumlah;

                        no++;
                        row++;
                    }
                }
            }
            else if(_config == "RL 5.2_Kunjungan Rawat Jalan")
            {
                var no = 1;
                var row = 2;
                using (var s = new SIMEntities())
                {
                    var dt = s.RL_5_2(_fromdate, _todate).ToList();
                    foreach (var x in dt)
                    {
                        //Kode_RS Nama_RS Bulan Tahun   Nama_KabKota Kode_Prov   Nomor JenisKegiatan   JML
                        exl.Cells[$"A{row}"].Value = x.Kode_RS;
                        exl.Cells[$"B{row}"].Value = x.Nama_RS;
                        exl.Cells[$"C{row}"].Value = x.Bulan;
                        exl.Cells[$"D{row}"].Value = x.Tahun;
                        exl.Cells[$"E{row}"].Value = x.Nama_KabKota;
                        exl.Cells[$"F{row}"].Value = x.Kode_Prov;
                        exl.Cells[$"G{row}"].Value = x.Nomor;
                        exl.Cells[$"H{row}"].Value = x.JenisKegiatan;
                        exl.Cells[$"I{row}"].Value = x.JML;

                        no++;
                        row++;
                    }
                }
            }
            else if (_config == "RL 5.3 10_Besar Penyakit Rawat Inap")
            {
                var no = 1;
                var row = 2;
                using (var s = new SIMEntities())
                {
                    var dt = s.RL_5_3(_fromdate, _todate).ToList();
                    foreach (var x in dt)
                    {
                        //Kode_Prov	Nama_KabKota	Kode_RS	Nama_RS	Bulan	Tahun	Nomor	KodeICD	Deskripsi	Keluar_Hidup_L	Keluar_Hidup_P	Keluar_Mati_L	Keluar_Mati_P	Total_All
                       
                        exl.Cells[$"A{row}"].Value = x.Kode_Prov;
                        exl.Cells[$"B{row}"].Value = x.Nama_KabKota;
                        exl.Cells[$"C{row}"].Value = x.Kode_RS;
                        exl.Cells[$"D{row}"].Value = x.Nama_RS;
                        exl.Cells[$"E{row}"].Value = x.Bulan;
                        exl.Cells[$"F{row}"].Value = x.Tahun;
                        exl.Cells[$"G{row}"].Value = x.Nomor;
                        exl.Cells[$"H{row}"].Value = x.KodeICD;
                        exl.Cells[$"I{row}"].Value = x.Deskripsi;
                        exl.Cells[$"J{row}"].Value = x.Keluar_Hidup_L;
                        exl.Cells[$"K{row}"].Value = x.Keluar_Hidup_P;
                        exl.Cells[$"L{row}"].Value = x.Keluar_Mati_L;
                        exl.Cells[$"M{row}"].Value = x.Keluar_Mati_P;
                        exl.Cells[$"N{row}"].Value = x.Total_All;

                        no++;
                        row++;
                    }
                }
            }
            else if (_config == "RL 5.4 10_Besar Penyakit Rawat Jalan")
            {
                var no = 1;
                var row = 2;
                using (var s = new SIMEntities())
                {
                    var dt = s.RL_5_4(_fromdate, _todate).ToList();
                    foreach (var x in dt)
                    {
                        exl.Cells[$"A{row}"].Value = x.Kode_Prov;
                        exl.Cells[$"B{row}"].Value = x.Nama_KabKota;
                        exl.Cells[$"C{row}"].Value = x.Kode_RS;
                        exl.Cells[$"D{row}"].Value = x.Nama_RS;
                        exl.Cells[$"E{row}"].Value = x.Bulan;
                        exl.Cells[$"F{row}"].Value = x.Tahun;
                        exl.Cells[$"G{row}"].Value = x.Nomor;
                        exl.Cells[$"H{row}"].Value = x.KodeICD;
                        exl.Cells[$"I{row}"].Value = x.Deskripsi;
                        exl.Cells[$"J{row}"].Value = x.Kasus_Baru_L;
                        exl.Cells[$"K{row}"].Value = x.Kasus_Baru_P;
                        exl.Cells[$"L{row}"].Value = x.Kasus_Baru_Total;
                        exl.Cells[$"M{row}"].Value = x.JML_Kunjungan;

                        no++;
                        row++;
                    }
                }
            }
            return exl;
        }

        public List<ExcelInfoViewModel> declareReport()
        {
            var m = new List<ExcelInfoViewModel>()
            {
                new ExcelInfoViewModel()
                {
                    param = "RL 5.1_Pengunjung SIRS",
                    title = $"RL 5.1_Pengunjung SIRS - {_fromdate.ToString("dd-MM-yyyy")} - {_todate.ToString("dd-MM-yyyy")}",
                    filename = "RL 5.1_Pengunjung SIRS.xlsx",
                    worksheet = "Data",
                    report_init = "",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.2_Kunjungan Rawat Jalan",
                    title = $"RL 5.2_Kunjungan Rawat Jalan - {_fromdate.ToString("dd-MM-yyyy")} - {_todate.ToString("dd-MM-yyyy")}",
                    filename = "RL 5.2_Kunjungan Rawat Jalan.xlsx",
                    worksheet = "Data",
                    report_init = "",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.3 10_Besar Penyakit Rawat Inap",
                    title = $"RL 5.3 10_Besar Penyakit Rawat Inap - {_fromdate.ToString("dd-MM-yyyy")} - {_todate.ToString("dd-MM-yyyy")}",
                    filename = "RL 5.3 10_Besar Penyakit Rawat Inap.xlsx",
                    worksheet = "Data",
                    report_init = "",
                },
                new ExcelInfoViewModel()
                {
                    param = "RL 5.4 10_Besar Penyakit Rawat Jalan",
                    title = $"RL 5.4 10_Besar Penyakit Rawat Jalan.xls - {_fromdate.ToString("dd-MM-yyyy")} - {_todate.ToString("dd-MM-yyyy")}",
                    filename = "RL 5.4 10_Besar Penyakit Rawat Jalan.xlsx",
                    worksheet = "Data",
                    report_init = "",
                },
            };

            return m;
            
        }
    }
    
    public class ExcelInfoViewModel
    {
        public string param { get; set; }
        public string title { get; set; }
        public string filename { get; set; }
        public string worksheet { get; set; }
        public string report_init { get; set; }
    }
}