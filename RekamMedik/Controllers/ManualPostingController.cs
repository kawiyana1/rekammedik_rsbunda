﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using RekamMedik.Entities;
using RekamMedik.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Configuration;

namespace RekamMedik.CrystalReports
{
    public class ManualPostingController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_ManualPosting().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg = null, int pembayaran = 0)
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var id = user.GetUserId();

            var model = new ManualPostingViewModel();
            using (var s = new SIMEntities())
            {
                if (noreg != null)
                {
                    var dtreg = s.Kasir_GetBillingPasien.Where(x => x.NoReg == noreg).FirstOrDefault();
                    if (dtreg != null)
                    {
                        model.NoReg = dtreg.NoReg;
                        model.TglReg = dtreg.TglReg.ToString("yyyy-MM-dd");
                        model.NRM = dtreg.NRM;
                        model.NamaPasien = dtreg.NamaPasien;
                        model.Alamat = dtreg.Alamat;
                        model.Registrasi_JenisKerjasama = dtreg.JenisKerjasama;
                        model.Registrasi_CompanyName = dtreg.Nama_Customer;
                    }
                }

                model.SectionID = WebConfigurationManager.AppSettings["SectionIDRekamMedis"];
                var collect_section = s.SIMmSection.FirstOrDefault(e => e.SectionID == model.SectionID);
                if (collect_section != null)
                {
                    model.SectionIDName = collect_section.SectionName;
                }
            }
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            model.Pembayaran = pembayaran;
            

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new ManualPostingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var userid = User.Identity.GetUserId();
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                var nobukti = item.NoBukti;
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var m = new SIMtrRJ()
                                {
                                    NoBukti = nobukti,
                                    RawatInap = simtrregistrasi.RawatInap,
                                    RegNo = item.NoReg,
                                    JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID,
                                    Tanggal = item.Tanggal,
                                    Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                    SectionID = item.SectionID,
                                    DokterID = item.DokterID,
                                    KdKelas = simtrregistrasi.KdKelas,
                                    UserID = 1103,
                                    NRM = simtrregistrasi.NRM,
                                    Status = "O",
                                    MCU = false,
                                    Batal = false,
                                    AdaObat = false,
                                    Audit = false,
                                    ManualPosting = true,
                                    UserIDWeb = userid
                                };
                                s.SIMtrRJ.Add(m);
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon,
                                            Pajak = 0                                            
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {m.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true,
                                    Data = nobukti
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            ManualPostingViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                #region ====  G E T  A L L  D A T A
                var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                var pasien = s.Kasir_GetBillingPasien_all.Where(x => x.NoReg == m.RegNo).FirstOrDefault();
                #endregion

                model = new ManualPostingViewModel()
                {
                    Tanggal = m.Tanggal,
                    Jam = m.Jam,
                    DokterID = dokter.DokterID ?? dokter.DokterID,
                    NamaDokter = dokter.NamaDOkter ?? dokter.NamaDOkter,
                    SectionID = section.SectionID ?? section.SectionID,
                    Nama_asli = section.SectionName ?? section.SectionName,
                    KdKelas = kelas.KelasID ?? kelas.KelasID,
                    NoReg = pasien.NoReg ?? pasien.NoReg,
                    TglReg = pasien.TglReg.ToString("dd-MM-yyyy"),
                    NRM = pasien.NRM ?? pasien.NRM,
                    NamaPasien = pasien.NamaPasien ?? pasien.NamaPasien,
                    Alamat = pasien.Alamat ?? pasien.Alamat,
                    Registrasi_JenisKerjasama = pasien.JenisKerjasama ?? pasien.JenisKerjasama,
                    Registrasi_CompanyName = pasien.Nama_Customer ?? pasien.Nama_Customer,
                    AdaObat = m.AdaObat,
                    Audit = m.Audit,
                    detail = new List<ManualPostingInsertDetailViewModel>()
                };


                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new ManualPostingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.NoReg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.SectionID,
                        tarif = x.Tarif,
                        detail = new List<ManualPostingInsertDetailJasaViewModel>()
                    };
                    model.Jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new ManualPostingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new ManualPostingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var nobukti = item.NoBukti;
                                var userid = User.Identity.GetUserId();
                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat di ubah");

                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");
                                if (simtrregistrasi.StatusBayar == "Sudah Bayar") throw new Exception("Pasien sudah melakukan pembayaran");

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.NoBukti);
                                if (SIMtrRJTransaksiDetail != null) { 
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.NoBukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                #endregion

                                SIMtrRJ.Tanggal = item.Tanggal;
                                SIMtrRJ.Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay;
                                SIMtrRJ.SectionID = item.SectionID;
                                SIMtrRJ.DokterID = item.DokterID;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.MCU = false;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.ManualPosting = true;
                                SIMtrRJ.UserIDWeb = userid;
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== D E T A I L
        [HttpGet]
        public ActionResult Detail(string nobukti, int pembayaran = 0)
        {
            ManualPostingViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var r = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.RegNo);
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                #region ====  G E T  A L L  D A T A
                var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                var pasien = s.Kasir_GetBillingPasien_all.Where(x => x.NoReg == m.RegNo).FirstOrDefault();
                #endregion

                model = new ManualPostingViewModel()
                {
                    Tanggal = m.Tanggal,
                    Jam = m.Jam,
                    DokterID = dokter.NamaDOkter,
                    SectionID = section.SectionName ?? section.SectionName,
                    KdKelas = kelas.KelasID ?? kelas.KelasID,
                    NoReg = pasien.NoReg ?? pasien.NoReg,
                    TglReg = pasien.TglReg.ToString("dd-MM-yyyy"),
                    NRM = pasien.NRM ?? pasien.NRM,
                    NamaPasien = pasien.NamaPasien ?? pasien.NamaPasien,
                    Alamat = pasien.Alamat ?? pasien.Alamat,
                    Registrasi_JenisKerjasama = pasien.JenisKerjasama ?? pasien.JenisKerjasama,
                    Registrasi_CompanyName = pasien.Nama_Customer ?? pasien.Nama_Customer,
                    AdaObat = m.AdaObat,
                    Audit = m.Audit,
                    Pulang = (r.StatusBayar == "Sudah Bayar") ? 1 : 0,
                    detail = new List<ManualPostingInsertDetailViewModel>()
                };


                

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new ManualPostingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.NoReg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.SectionID,
                        tarif = x.Tarif,
                        detail = new List<ManualPostingInsertDetailJasaViewModel>()
                    };
                    model.Jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new ManualPostingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }

            model.Pembayaran = pembayaran;
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region === T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var section = WebConfigurationManager.AppSettings["SectionIDRekamMedis"];
                    IQueryable<Kasir_ListManualPosting> proses = s.Kasir_ListManualPosting.Where(e => e.SectionID == section);
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.Registrasi_NoReg)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NamaDokter)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.Registrasi_JenisKerjasama)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ManualPostingViewModel>(x));
                    foreach (var x in m)
                    {
                        var r = s.SIMtrRegistrasi.FirstOrDefault(yx => yx.NoReg == x.Registrasi_NoReg);
                        if(r != null)
                        {
                            x.Pulang = (r.StatusBayar == "Sudah Bayar") ? 1 : 0;
                        }

                        x.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    //var sectionID = "";
                    //if (Request.Cookies["SectionID"] != null)
                    //{
                    //    if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                    //    {
                    //        sectionID = Request.Cookies["SectionID"].Value;
                    //    }
                    //}

                    IQueryable<Kasir_GetBillingPasien> proses = s.Kasir_GetBillingPasien;
                    if (filter[14] == "True")
                    {
                        proses = proses.Where(x => x.ProsesPayment == true);
                    }

                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NRM)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NamaPasien)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.Nama_Customer)}.Contains(@0)", filter[3]);

                    //if (sectionID == "SEC080" && !string.IsNullOrEmpty(sectionID))
                    //{
                    //    proses = proses.Where(x => x.TipePelayanan == "RI");
                    //}

                    //if (sectionID == "SEC079" && !string.IsNullOrEmpty(sectionID))
                    //{
                    //    proses = proses.Where(x => x.TipePelayanan != "RI");
                    //}
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mDokter>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListKelas(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmKelas> proses = s.SIMmKelas;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmKelas.KelasID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmKelas.NamaKelas)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(SIMmKelas.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmKelas>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, kategorioperasi, section, noreg, "1").FirstOrDefault();
                    if (h == null) throw new Exception("Jasa tidak ditemukan, GetTarifBiaya_Global tidak ditemukan");
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : WebConfigurationManager.AppSettings["SectionIDRekamMedis"]);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrRJ Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== GET DATA REGISTRASI
        [HttpGet]
        public string GetDataRegistrasi(string id)
        {
            var m = new Kasir_GetBillingPasien();
            var result = new RegistrasiViewModel();
            using (var s = new SIMEntities())
            {
                m = s.Kasir_GetBillingPasien.FirstOrDefault(x => x.NoReg == id);
            }
            if (m != null)
            {
                result = IConverter.Cast<RegistrasiViewModel>(m);
                result.Deposit_View = m.Deposit == null ? "0" : ((decimal)m.Deposit).ToMoney();
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }

        private bool checkDetailKomponen(ManualPostingViewModel item)
        {

            int error = 0;
            if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
            if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
            foreach (var x in item.detail)
            {
                decimal sumHarga = 0;
                if (x.detail == null) throw new Exception("Detail komponen masih kosong");

                foreach (var y in x.detail)
                {
                    sumHarga += y.harga;
                }
                error += (x.tarif == sumHarga) ? 0 : 1;
            }
            if (error > 0)
            {
                throw new Exception("Harga detail tidak sama dengan harga header");
            }
            return true;
        }
        #endregion
    }
}