﻿using Microsoft.AspNet.Identity;
using RekamMedik.Entities;
using RekamMedik.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using Newtonsoft.Json;
using System.Configuration;
using RekamMedik.Helper;

namespace RekamMedik.Controllers
{
    [Authorize(Roles = "RekamMedik")]
    public class ReportController : Controller
    {
        private string serverpath = "~/CrystalReports/";
        private string category = "RawatJalan";

        public ActionResult Index(string dir = "RawatJalan")
        {
            category = dir;
            var hreport = new HReport(Server, serverpath);
            var r = new ReportHelperViewModel()
            {
                Name = category,
                Reports = hreport.List(category),
                UserID = User.Identity.GetUserId(),
                UserName = User.Identity.GetUserName()
            };
            return View(r);
        }

        public ActionResult ExportPDF(string filename, string param, string directory = "RawatJalan")
        {
            category = directory;
            try
            {
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["SIMConnectionString"].ConnectionString.Split(';'))
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportPDF(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}