﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class ListDetail<TModel> : List<DetailState<TModel>>
    {
        public IEnumerable<TModel> Model { get { return this.Select(x => x.Model); } }

        public ListDetail()
        {

        }

        public ListDetail(List<DetailState<TModel>> list)
        {
            list.ForEach(x => this.Add(x));
        }

        public void Add(bool hide, TModel model)
        {
            this.Add(new DetailState<TModel>(hide, model));
        }

        public List<TModel> GetModel(bool remove)
        {
            return this.Where(x => x.Remove == remove).Select(y => y.Model).ToList();
        }
    }

    public class DetailState<TModel>
    {
        public bool Remove { get; set; }
        public TModel Model { get; set; }

        public DetailState() { }

        public DetailState(bool remove, TModel model)
        {
            this.Remove = remove;
            this.Model = model;
        }
    }
}
